import React from "react";
import useStyles from "./styles";

interface TrendingSectionProps {
  videos: Array<{
    Id: string;
    Title: string;
    CoverImage: string;
    TitleImage: string;
    Date: string;
    ReleaseYear: string;
    MpaRating: string;
    Category: string;
    Duration: string;
    VideoUrl: string;
    Description: string;
  }>;
  onViewMoreClick: (video: any) => void;
}

const TrendingSection: React.FC<TrendingSectionProps> = ({ videos, onViewMoreClick }) => {
  const classes = useStyles();

  return (
    <div className={classes.trendingSectionContainer}>
      <h2>Trending Now</h2>
      <div className={classes.carouselContainer}>
        {videos.slice(0, 8).map((video) => (
          <div
            key={video.Id}
            className={classes.carouselItem}
            onClick={() => onViewMoreClick(video)}
          >
            <img
              src={`/assets/images/${video.CoverImage}`}
              alt={video.Title}
              className={classes.carouselItemImage}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default TrendingSection;
