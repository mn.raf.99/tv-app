import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  trendingSectionContainer: {
    padding: "20px",
    backgroundColor: "#f0f0f0",

    "@media (max-width: 768px)": {
      padding: "10px",
    },
  },

  carouselContainer: {
    display: "flex",
    overflow: "hidden",
    margin: "0 -10px",

    "@media (max-width: 768px)": {
      margin: "0",
      overflowX: "auto",
      flexWrap: "nowrap",
      scrollSnapType: "x mandatory",
    },
  },

  carouselItem: {
    boxSizing: "border-box",
    padding: "0 10px",
    cursor: "pointer",

    "@media (max-width: 768px)": {
      flex: "0 0 auto",
      scrollSnapAlign: "start",
    },
  },

  carouselItemImage: {
    width: "100%",
    height: "auto",
  },
});

export default useStyles;
