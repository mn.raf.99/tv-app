import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  mainMenu: {
    position: "absolute",
    flexDirection: "column",
    zIndex: "50",
    display: "flex",
    gap: "45px",
    top: "16%",
    left: "0%",

    padding: "15px",
  },
  menuItem: {
    width: "30px",
    height: "30px",
  },
  menuContainer: {
    display: "flex",
    alignItems: "center",
    position: "relative",
    gap: "50px",
  },
  additionalMenu: {
    color: "white",
    fontSize: "30px",
  },

  bottom: {
    color: "white",
    top: "100%",
  },
});

export default useStyles;
