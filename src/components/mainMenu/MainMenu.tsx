import React, { useState } from "react";
import useStyles from "./styles";

const MainMenu: React.FC = () => {
  const classes = useStyles();
  const [isHovered, setIsHovered] = useState(false);
  let timeoutId: number;

  const handleHover = (enter: boolean) => {
    clearTimeout(timeoutId);

    if (enter) {
      setIsHovered(true);
    } else {
      timeoutId = window.setTimeout(() => {
        setIsHovered(false);
      }, 500);
    }
  };

  const iconWidthPercentage = 10;

  return (
    <div
      className={classes.mainMenu}
      onMouseEnter={() => handleHover(true)}
      onMouseLeave={() => handleHover(false)}
      style={{
        left: isHovered ? `${iconWidthPercentage}%` : "1%",
        background: isHovered ? "black" : "rgba(0, 0, 0, 0)",
        transition: "left 1s ease-in-out, background 1s ease-in-out",
      }}
    >
      <div className={classes.menuContainer}>
        <img
          src="assets/images/icons/ICON - Search.png"
          alt="Search Icon"
          className={classes.menuItem}
        />
        {isHovered && (
          <div className={classes.additionalMenu}>
            <div>Search</div>
          </div>
        )}
      </div>
      <div className={classes.menuContainer}>
        <img
          src="assets/images/icons/Group 46.png"
          alt="Search Icon"
          className={classes.menuItem}
        />
        {isHovered && (
          <div className={classes.additionalMenu}>
            <div>Home</div>
          </div>
        )}
      </div>
      <div className={classes.menuContainer}>
        <img
          src="assets/images/icons/Group 56.png"
          alt="Search Icon"
          className={classes.menuItem}
        />
        {isHovered && (
          <div className={classes.additionalMenu}>
            <div>TV Shows</div>
          </div>
        )}
      </div>
      <div className={classes.menuContainer}>
        <img
          src="assets/images/icons/Group 54.png"
          alt="Search Icon"
          className={classes.menuItem}
        />
        {isHovered && (
          <div className={classes.additionalMenu}>
            <div>Movies</div>
          </div>
        )}
      </div>
      <div className={classes.menuContainer}>
        <img
          src="assets/images/icons/Group 53.png"
          alt="Search Icon"
          className={classes.menuItem}
        />
        {isHovered && (
          <div className={classes.additionalMenu}>
            <div>Genres</div>
          </div>
        )}
      </div>
      <div className={classes.menuContainer}>
        <img
          src="assets/images/icons/Group 47.png"
          alt="Search Icon"
          className={classes.menuItem}
        />
        {isHovered && (
          <div className={classes.additionalMenu}>
            <div>Watch Later</div>
          </div>
        )}
      </div>

      {isHovered && (
        <div className={classes.bottom}>
          <p>Language</p>
          <p>Get Help</p>
          <p> Exit</p>
        </div>
      )}
    </div>
  );
};

export default MainMenu;
