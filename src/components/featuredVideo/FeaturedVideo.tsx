import React, { useEffect, useState } from "react";
import useStyles from "./styles";

interface FeaturedVideoProps {
  video: {
    Id: string;
    Title: string;
    CoverImage: string;
    TitleImage: string;
    Date: string;
    ReleaseYear: string;
    MpaRating: string;
    Category: string;
    Duration: string;
    VideoUrl?: string;
    Description: string;
  };
  onPlayClick: () => void;
  onMoreInfoClick: () => void;
  onBackgroundChange: (videoUrl: string) => void;
}

const FeaturedVideo: React.FC<FeaturedVideoProps> = ({ video, onPlayClick, onMoreInfoClick }) => {
  const classes = useStyles();

  const [isVideoPlaying, setIsVideoPlaying] = useState(false);

  useEffect(() => {
    if (video.VideoUrl && isVideoPlaying) {
      const timeoutId = setTimeout(() => {
        if (video.VideoUrl) {
        }
      }, 2000);

      return () => clearTimeout(timeoutId);
    }
  }, [video, isVideoPlaying]);

  const handlePlayClick = () => {
    setIsVideoPlaying(true);
    onPlayClick();
  };

  return (
    <div className={classes.featuredVideoContainer}>
      {isVideoPlaying ? (
        <video
          autoPlay
          muted
          loop
          playsInline
          src={video.VideoUrl}
          style={{ width: "100%" }}
          onClick={handlePlayClick}
        />
      ) : (
        <>
          <img
            src={`/assets/images/${video.CoverImage}`}
            alt={video.Title}
            className={classes.coverImage}
            onClick={handlePlayClick}
          />
          <div className={classes.videoDetails}>
            <h1 className={classes.category}>{video.Category}</h1>
            <img src={`/assets/images/${video.TitleImage}`} alt={video.Title} />

            <div className={classes.detailsRow}>
              <p>{video.ReleaseYear}</p>
              <p>{video.MpaRating}</p>
              <p>{video.Duration}</p>
            </div>
            <p className={classes.description}>{video.Description}</p>
            <br />
            <div className={classes.buttons}>
              <button onClick={handlePlayClick} className={classes.playButton}>
                Play
              </button>
              <button onClick={onMoreInfoClick} className={classes.moreButton}>
                More Info
              </button>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default FeaturedVideo;
