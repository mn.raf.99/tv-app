import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  featuredVideoContainer: {
    position: "relative",
    zIndex: 40,
  },

  coverImage: {
    width: "100%",
    height: "1000px",
  },

  category: {
    color: "#696969",
  },

  videoDetails: {
    position: "absolute",
    top: "15%",
    left: "10%",
    color: "#fff",
    display: "flex",
    flexDirection: "column",
  },

  detailsRow: {
    display: "flex",
    alignItems: "center",
    gap: "40px",
    fontSize: "30px",
  },

  description: {
    fontSize: "40px",
    margin: "0",
  },

  buttons: {
    display: "flex",
    gap: "20px",
  },

  playButton: {
    fontSize: "30px",
    padding: "20px 30px",
    textAlign: "center",
    borderRadius: "50px",
    backgroundColor: "white",
    color: "black",
    cursor: "pointer",
    width: "200px",
  },

  moreButton: {
    fontSize: "30px",
    padding: "20px 30px",
    textAlign: "center",
    borderRadius: "50px",
    backgroundColor: "#0000FF",
    color: "#fff",
    cursor: "pointer",
  },

  "@media (max-width: 1200px)": {
    coverImage: {
      height: "800px",
    },
    videoDetails: {
      top: "10%",
      left: "2%",
      fontSize: "30px",
    },
    detailsRow: {
      fontSize: "30px",
      gap: "30px",
    },
    description: {
      fontSize: "30px",
    },
    playButton: {
      fontSize: "25px",
      padding: "15px 20px",
    },
    moreButton: {
      fontSize: "25px",
      padding: "15px 20px",
    },
  },

  "@media (max-width: 768px)": {
    coverImage: {
      height: "600px",
    },
    videoDetails: {
      top: "5%",
      left: "2%",
      fontSize: "20px",
    },
    detailsRow: {
      fontSize: "20px",
      gap: "20px",
    },
    description: {
      fontSize: "20px",
    },
    playButton: {
      fontSize: "18px",
      padding: "10px 15px",
    },
    moreButton: {
      fontSize: "18px",
      padding: "10px 15px",
    },
  },
});

export default useStyles;
