import React from "react";
import videoData from "./data/videoData.json";
import MainMenu from "./components/mainMenu";
import FeaturedVideo from "./components/featuredVideo";
import TrendingSection from "./components/trendingSection";

const App: React.FC = () => {
  const [lastClickedVideo, setLastClickedVideo] = React.useState(videoData.Featured);

  const handleVideoClick = (video: any) => {
    sessionStorage.setItem("lastClickedVideo", JSON.stringify(video));
    setLastClickedVideo(video);
  };

  const handleBackgroundChange = (videoUrl: string) => {
    console.log("Changing background to:", videoUrl);
  };

  return (
    <div>
      <MainMenu />
      <FeaturedVideo
        video={lastClickedVideo}
        onPlayClick={() => handleVideoClick(lastClickedVideo)}
        onMoreInfoClick={() => handleVideoClick(lastClickedVideo)}
        onBackgroundChange={handleBackgroundChange}
      />
      <TrendingSection
        videos={videoData.TendingNow}
        onViewMoreClick={(video) => handleVideoClick(video)}
      />
    </div>
  );
};

export default App;
